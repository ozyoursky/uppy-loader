import Vue from 'vue'
import App from './App.vue'
import router from './js/plugins/route'
import vuetify from './js/plugins/vuetify';
import './js/plugins/vue-mask'
import '@babel/polyfill'

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
