import CredentialService from './CredentialService'
import axios from 'axios'

/**
 *
 *@class OrderService
 *
 * @property {order-handler} root
 * @property {order-form} form
 * @property {uppy-loader} fileloader
 */

export default class OrderService {
  constructor(form, fileloader) {
    this.form = form
    this.fileloader = fileloader
  }

  submitOrder() {
    if (this.form.validate()) {
      this.fileloader.upload().then(res => {
          if (res.failed.length === 0)
            this.uploadOrderInfo()
        },
      )
    }
  }

  uploadOrderInfo() {
    const content =`${new Date()}\n`+ this.form.getHumanInfo()
    console.log(content)
    CredentialService.getPresignedUrl('orderInfo.txt')
      .then(url => {
          axios.put(url.data, content)
            .catch(reason => {
                console.log(reason)
              },
            )
        },
      )
  }


}