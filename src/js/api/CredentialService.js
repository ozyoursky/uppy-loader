import axios from 'axios'

export default class CredentialService {
  static getPresignedUrl(name) {
    return axios.get(`${CredentialService.serverUrl}/signedUrl?filename=${name}&folder=${CredentialService.bucketFolder}`)
  }

  static getTempToken(password){
    return axios(`${CredentialService.serverUrl}/awstoken?pwd=${password}`)
  }
}