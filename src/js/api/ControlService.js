import AWS from 'aws-sdk'
import CredentialService from './CredentialService'
//TODO realise:
// get all folders
// mark folder(order) as done by file or property
// delete/download marked
// show by prop: done / active

export default class ControlService {
  s3Client

  async init(password) {
    //TODO redo with google authenticator
    return CredentialService.getTempToken(password).then(resp => {
      AWS.config.credentials = resp.data.credentials
      this.s3Client = new AWS.S3({region: 'eu-central-1'})
    })
  }

  async getAllorders() {
    return new Promise((resolve, reject) => {
        this.s3Client.listObjectsV2({Bucket: 'photoloader-bucket'}, (err, data) => {
          resolve(data.Contents)
          reject(err)
        })
      },
    )
  }
}