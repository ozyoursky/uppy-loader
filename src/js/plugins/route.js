import Vue from 'vue'
import VueRouter from 'vue-router'
import ControlPanel from '../../components/control-panel/control-panel.component'
import OrderHandler from '../../components/order-handler/order-handler.component'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'order-handler',
    component: OrderHandler,
  },
  {
    path: '/control',
    name: 'control-panel',
    component: ControlPanel,
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

export default router
