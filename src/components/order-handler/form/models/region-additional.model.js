/**
 *@class RegionAdditional
 *
 * @property {String} label
 * @property {string} value
 * @property {Boolean} required
 */
export default class RegionAdditional {
  toString(){
    return `${this.label} : ${this.value}`
  }
}