/**
 * @class Region
 *
 * @property {String} type
 * @property {String} label
 * @property {Array<RegionAdditional>} additionals
 */
export default class Region {}