import { formLocales as locales } from '../form-settings'

/**
 * @class OrderForm
 *
 * @property {String} name
 * @property {String} phone
 * @property {String} address
 * @property {Region} region
 * @property {PrintType} printType
 * @property {PrintFormat} printFormat
 * @property {String} wishes
 */
export default class OrderForm {
  toHumanInfo() {  //TODO rework with toString
    // console.log(this.printFormat)
    return `
    ${locales.nameLabel} : ${this.name}
    ${locales.phone} : ${this.phone}
    ${locales.region} : ${this.region.label} ${this.region.additionals.length > 0 ?
      this.region.additionals
        .reduce((text, item) => {
          return text += '\n    ' + item.label + ' : ' + item.value
        }, '')
      : ''} 
    ${locales.address} : ${this.address}
    ${locales.printType} : ${this.printType.label}
    ${this.printType.formatLabel} : ${this.printFormat.label}
    ${locales.wishes} : ${this.wishes ? this.wishes : 'нет'}`
  }
}
