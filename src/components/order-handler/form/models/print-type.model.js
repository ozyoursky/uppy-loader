/**
 * @class PrintType
 *
 * @property {String} label
 * @property {String} formatLabel
 * @property {String} thumbnail
 * @property {Array<PrintFormat>} formats
 */
export default class PrintType {}
