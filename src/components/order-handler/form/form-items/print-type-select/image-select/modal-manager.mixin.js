export const ModalManagerMixin = {
  data() {
    return {
      isOpen: false,
      closeResolver: null,
    }
  },
  methods: {
    open() {
      this.isOpen = true
      return new Promise(resolve => this.closeResolver = resolve)
    },
    close() {
      this.isOpen = false
      this.closeResolver()
    },
  },
}
