/**
 * @class ImageSelectElement
 *
 * @property {String} imageUrl
 * @property {String} label
 * @property {Object} value
 */
export default class ImageSelectElement {
  constructor(imageUrl, label, value) {
    this.imageUrl = imageUrl
    this.label = label
    this.value = value
  }
}
