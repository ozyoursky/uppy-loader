export const formSettings = {
  printTypes: [
    {
      label: 'Фото',
      formatLabel: 'Размер фото',
      thumbnail: 'https://photoloader-bucket.s3.eu-central-1.amazonaws.com/photo-type.jpg',
      formats: [
        {
          label: 'стандарт(10х15)',
          price: 0.25,
        },
        {
          label: 'квадрат(10х10)',
          price: 0.35,
        },
        {
          label: 'polaroid(10x12)',
          price: 0.35,
        },
        {
          label: 'miniPolaroid(7x9)',
          price: 0.25,
        },
        {
          label: 'Другой, указать в примечании',
          price: 0,
        },
      ],
    },
    {
      label: 'Холст',
      formatLabel: 'Размер холста',
      thumbnail: 'https://photoloader-bucket.s3.eu-central-1.amazonaws.com/canvas-type.jpg',
      formats: [
        {
          label: '30x40',
          price: 25,
        },
        {
          label: '40x40',
          price: 30,
        },
        {
          label: '40x55',
          price: 34,
        },
        {
          label: '55x55',
          price: 40,
        },
        {
          label: '50x70',
          price: 46,
        },
        {
          label: '55x80',
          price: 55,
        },
        {
          label: 'Другой, указать в примечании',
          price: 0,
        },
      ],
    },
    {
      label: 'Магниты',
      clarification: '(только по Гродно)',
      formatLabel: 'Размер магнитов',
      thumbnail: 'https://photoloader-bucket.s3.eu-central-1.amazonaws.com/magnet-type.jpg',
      formats: [
        {
          label: '5x7',
          price: 1.3,
        },
        {
          label: '7x7',
          price: 1.8,
        },
        {
          label: '7x9(miniPolar)',
          price: 2,
        },
        {
          label: 'Другой, указать в примечании',
          price: 0,
        },
      ],
    },
  ],
  regions: [
    {
      type: 'GRODNO',
      label: 'Гродно(курьер)',
      additionals: [],
    },
    {
      type: 'BY',
      label: 'Вся Беларусь(Почтой)',
      additionals: [
        {
          label: 'Почтовый индекс',
          required: true,
        },
        {
          label: 'Имя получателя',
          required: true,
        },
      ],
    },
  ],
}

export const formLocales = {
  nameLabel: 'Для обратной связи',
  namePlaceholder: 'Ник в Instagram',
  region: 'Регион',
  address: 'Адрес',
  phone: 'Телефон',
  printType: 'Тип печати',
  wishes: 'Примечания',
}
